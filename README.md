# Sim 800L test

**GSM module test**

This project tests the GSM module, using the Arduino IDE to upload source to NodeMCU please install the following:
1. Arduino IDE 1.8.15 or higher ([Download here](https://www.arduino.cc/en/software))



**Hardware used**
1. GSM Module (SIM 800L)
1. NodeMCU Esp12e
