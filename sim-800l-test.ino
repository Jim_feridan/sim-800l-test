#include "nodemcu_pins.h"
#include "sim_variables.h"
#include <SoftwareSerial.h>

SoftwareSerial GSMSerial(D2, D1);  // RX, TX

void setup() {
  Serial.begin(9600);
  GSMSerial.begin(9600);

  Serial.println("Initializing the serial");
  GSMSerial.println("AT");
  readGSM();
  GSMSerial.println("AT+CMGF=1");
  readGSM();
  GSMSerial.println("AT+CNMI=1,2,0,0,0");
  readGSM();
  Serial.println("Done");
}

void loop() {
  delay(500);
  String msg = readSerial();
  if( msg != ""){
    GSMSerial.println(msg);
    Serial.println(msg);
  }  
  readGSM();
}

String readSerial(){
  String msg = "";
  while (Serial.available()){
    msg += (char)Serial.read();
  }
  msg.trim();
  return msg;
}

void sendToGSM(String msg){
  GSMSerial.println(msg);
}

void readGSM(){
  String msg = "";
  while (GSMSerial.available()){
    msg += (char)GSMSerial.read();
  }
  if (msg != ""){
    msg.trim();
    Serial.print("Recieved from gsm ");
    Serial.println(msg);
  }
}
